import * as modules from './admin/modules';
import * as packages from './admin/_packajes';

require('../css/admin.scss');

window.app = Object.assign(modules, packages);
