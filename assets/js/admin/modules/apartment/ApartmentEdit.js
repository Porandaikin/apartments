import Vue from 'vue';
import ApartmentEditComponent from './../vue-components/apartment/ApartmentEditComponent';

export class ApartmentEdit {
    constructor() {
        new Vue({
            el: '#editApartmentComponent',
            components: {
                ApartmentEditComponent
            }
        })
    }
}