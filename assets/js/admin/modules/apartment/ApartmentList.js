import Vue from 'vue/dist/vue.js';
import ApartmentListComponent from '../vue-components/apartment/ApartmentListComponent';

export class ApartmentList {
    constructor(){
        new Vue({
            el: '#apartmentListComponent',
            render: h => h(ApartmentListComponent)
        })
    }
}