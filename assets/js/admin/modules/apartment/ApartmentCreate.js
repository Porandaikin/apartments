import Vue from 'vue';
import ApartmentEditComponent from './../vue-components/apartment/ApartmentEditComponent';

export class ApartmentCreate {
    constructor() {
        new Vue({
            el: '#createApartmentComponent',
            components: {
                ApartmentEditComponent
            }
        })
    }
}