import axios from "axios/index";

export class ApartmentLayout {
    static getLayout(id, succsessCallback) {
        axios.get(`/api/apartment/getLayout/${id}/`).then((response) => {
            succsessCallback(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }
}
