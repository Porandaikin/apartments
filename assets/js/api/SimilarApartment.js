import axios from "axios/index";

export class SimilarApartment {
    static getSimilarApartments(id, succsessCallback) {
        axios.get(`/api/similar/getSimilarApartments/${id}/`).then((response) => {
            succsessCallback(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }

    static getNotSimilarApartments(id, succsessCallback) {
        axios.get(`/api/similar/getNotSimilarApartments/${id}/`).then((response) => {
            succsessCallback(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }
}