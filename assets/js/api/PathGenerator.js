export class PathGenerator {
    static getPathEditApartment(apartmentId) {
        return `/admin/apartment/edit/${apartmentId}/`
    }

    static getPathSaveApartment() {
        return '/admin/apartment/create';
    }
}