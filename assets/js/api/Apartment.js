import axios from "axios/index";

export class Apartment {
    static getApartments(filter = {}, successCallback) {
        axios.post('/api/apartaments', filter).then((response) => {
            successCallback(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }

    static getDataApartment(id, succsessCallback) {
        axios.get(`/api/apartament/${id}/`).then((response) => {
            succsessCallback(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }
}