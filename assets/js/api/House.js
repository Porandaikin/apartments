import axios from "axios/index";

export class House {
    static getAllHouses(successCallback) {
        axios.post('/api/house/list', {}).then((response) => {
            successCallback(response.data);
        }).catch((error) => {
            console.log(error);
        });
    }
}