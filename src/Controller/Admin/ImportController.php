<?php

namespace App\Controller\Admin;

use App\Config\Apartment\Room;
use App\Config\Apartment\Status;
use App\Config\Apartment\Type;
use App\Entity\Apartment;
use App\Entity\House;
use App\Repository\ApartmentRepository;
use App\Repository\HouseRepository;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\RowCellIterator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    /**
     * @Route("/admin/import", name="admin_import", methods={"GET"})
     * @param Filesystem $filesystem
     * @return Response
     * @throws \Exception
     */
    public function index(Filesystem $filesystem): Response
    {
        $file = $filesystem->exists($this->getParameter('app.path.import_file'))
            ? new File($this->getParameter('app.path.import_file'))
            : null;

        return $this->render('admin/import/index.html.twig', [
            'filename'       => $file ? $file->getFilename() : '',
            'lastUpdateFile' => $file ? (new \DateTime())->setTimestamp($file->getMTime())->format('d.m.Y H:i:s') : '',
        ]);
    }

    /**
     * @Route("/admin/import/getFile", name="admin_import_get_file")
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse|null
     */
    public function getFileImport(): ?\Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $file = new File($this->getParameter('app.path.import_file'));

        return $this->file($file);
    }

    /**
     * @Route("/admin/import", name="admin_import_save", methods={"POST"})
     * @param Request $request
     * @param ApartmentRepository $apartmentRepository
     * @param HouseRepository $houseRepository
     * @param Filesystem $filesystem
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws Exception
     */
    public function save(Request $request, ApartmentRepository $apartmentRepository, HouseRepository $houseRepository, Filesystem $filesystem): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        $fileImport = $request->files->get('fileImport', null);
        if ($fileImport !== null) {
            $pathToSaveImportFile = $this->getParameter('app.path.import_file');
            $filesystem->remove($pathToSaveImportFile);
            $filesystem->copy($fileImport->getRealPath(), $pathToSaveImportFile);
            $reader = new Xlsx();
            $spreadsheet = $reader->load($fileImport->getRealPath());
            foreach ($spreadsheet->getAllSheets() as $sheet) {
                $data = $sheet->toArray();
                $header = [];
                foreach ($data[0] as $keyHeader => $headerValue) {
                    if ($headerValue !== null) {
                        $header[$keyHeader] = $headerValue;
                    }
                }
                unset($data[0]);
                foreach ($data as $row) {
                    if ($row[0] === null) {
                        continue;
                    }
                    $numberApartment = (int)$row[0];
                    $floor = (int)$row[1];
                    $area = str_ireplace(',', '.', $row[2]);
                    $totalCost = (float)str_ireplace(',', '', $row[3]);
                    $quantityRoom = (int)$row[4];
                    $status = Status::getIntByName($row[5]);
                    $house = $houseRepository->findOneBy(['type' => $row[6]]);
                    //Подъезд
                    $costMeter = (float)str_ireplace(',', '', $row[8]);
                    //Куда выходят окна
                    $title = sprintf('%s %s кв. м2', Room::getNameByQuantityRoom($quantityRoom), $area);

                    if ($house === null) {
                        throw new \LogicException('Не найден дом: ' . $row[6]);
                    }
                    $manager = $this->getDoctrine()->getManager();
                    $apartment = $apartmentRepository->find($numberApartment);
                    if ($apartment === null) {
                        $apartment = new Apartment();
                    }
                    $apartment->setFloor($floor)
                        ->setArea($area)
                        ->setTotalCost($totalCost)
                        ->setQuantityRoom($quantityRoom)
                        ->setStatus($status)
                        ->setHouse($house)
                        ->setCostMeter($costMeter)
                        ->setTitle($title)
                        ->setType(Type::ROOMED);
                    if ($apartment->getId() === null) {
                        $apartment->setId($numberApartment);
                        $manager->persist($apartment);
                    }
                    $manager->flush();
                }
            }
        }
        return $this->redirect($this->generateUrl('admin_index'));
    }
}
