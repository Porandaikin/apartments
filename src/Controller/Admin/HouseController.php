<?php

namespace App\Controller\Admin;

use App\Entity\House;
use App\Repository\HouseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HouseController extends AbstractController
{
    /**
     * @Route("/admin/house", name="admin_house_list")
     * @param HouseRepository $houseRepository
     * @return Response
     */
    public function index(HouseRepository $houseRepository): Response
    {
        return $this->render('admin/house/list.html.twig', [
            'houses' => $houseRepository->findAll(),
        ]);
    }

    /**
     * @Route("/admin/house/create", name="admin_house_create", methods={"GET"})
     */
    public function create(): Response
    {
        return $this->render('admin/house/form.html.twig', [
            'house' => new House(),
        ]);
    }

    /**
     * @Route("/admin/house/create", name="admin_house_save", methods={"POST"})
     * @param Request $request
     * @param HouseRepository $houseRepository
     * @return RedirectResponse
     */
    public function save(Request $request, HouseRepository $houseRepository): RedirectResponse
    {
        $manager = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $typeHouse = $request->get('type_house');
        $quantityFloor = (int)$request->get('quantity_floor');
        try {
            $house = $houseRepository->find($id);
            if ($house === null) {
                $house = new House();
                $manager->persist($house);
            }
            $house->setType($typeHouse)
                ->setQuantityFloor($quantityFloor);
            $manager->persist($house);
            $manager->flush();
            $this->addFlash('notice', sprintf('Добавлени новый дом: %s', $typeHouse));
        } catch (\Throwable $exception) {
            $this->addFlash(
                'errors',
                sprintf('При добавлении нового дома %s возникла ошибка: %s', $typeHouse, $exception->getMessage())
            );
        }

        return $this->redirect($this->generateUrl('admin_house_list'));
    }

    /**
     * @Route("/admin/house/edit/{id}/", name="admin_house_edit", methods={"GET"})
     * @param int $id
     * @param HouseRepository $houseRepository
     * @return Response
     */
    public function edit(int $id, HouseRepository $houseRepository): Response
    {
        return $this->render('admin/house/form.html.twig', [
            'house' => $houseRepository->find($id),
        ]);
    }

    /**
     * @Route("/admin/house/delete/{id}/", name="admin_house_delete", methods={"GET"})
     * @param int $id
     * @param HouseRepository $houseRepository
     * @return RedirectResponse
     */
    public function delete(int $id, HouseRepository $houseRepository): RedirectResponse
    {
        $manager = $this->getDoctrine()->getManager();
        $house = $houseRepository->find($id);
        $manager->remove($house);
        $manager->flush();
        $this->addFlash('notice', sprintf('Удалён дом: %s', $house->getType()));

        return $this->redirect($this->generateUrl('admin_house_list'));
    }
}
