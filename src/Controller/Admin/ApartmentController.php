<?php

namespace App\Controller\Admin;

use App\Config\Apartment\Status as ApartmentStatus;
use App\Config\Apartment\Type as ApartmentType;
use App\Config\ApartmentLayout\Type as ApartmentLayoutType;
use App\Entity\Log;
use App\Dto\Entity\Apartment as ApartmentDto;
use App\Repository\ApartmentLayoutsRepository;
use App\Repository\ApartmentRepository;
use App\Repository\HouseRepository;
use App\Repository\SimilarApartmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApartmentController extends AbstractController
{
    /**
     * @Route("/admin/apartment/edit/{id}/", name="admin_apartment_edit", methods={"GET"}, requirements={"id"="\d+"})
     * @param int $id
     * @param HouseRepository $houseRepository
     * @return Response
     */
    public function edit(int $id, HouseRepository $houseRepository): Response
    {
        return $this->render('admin/apartment/edit.html.twig', [
            'houses'              => $houseRepository->findAll(),
            'apartmentsType'      => ApartmentType::getList(),
            'apartmentStatus'     => ApartmentStatus::getList(),
            'apartmentLayoutType' => ApartmentLayoutType::getList(),
            'id'                  => $id,
        ]);
    }

    /**
     * @Route("/admin/apartment/create", name="admin_apartment_create_form", methods={"GET"})
     * @param HouseRepository $houseRepository
     * @param ApartmentRepository $apartmentRepository
     * @return Response
     */
    public function create(HouseRepository $houseRepository, ApartmentRepository $apartmentRepository)
    {
        return $this->render('admin/apartment/create.html.twig', [
            'houses'              => $houseRepository->findAll(),
            'apartmentsType'      => ApartmentType::getList(),
            'apartmentStatus'     => ApartmentStatus::getList(),
            'apartmentLayoutType' => ApartmentLayoutType::getList(),
            'newId'               => $apartmentRepository->getMaxId() + 1,
        ]);
    }

    /**
     * @Route("/admin/apartment/create", name="admin_apartment_save_form", methods={"POST"})
     * @param Request $request
     * @param ApartmentLayoutsRepository $apartmentLayoutsRepository
     * @param SimilarApartmentRepository $similarApartmentsRepository
     * @param ApartmentDto $apartmentDto
     * @return RedirectResponse
     */
    public function save(
        Request $request,
        ApartmentLayoutsRepository $apartmentLayoutsRepository,
        SimilarApartmentRepository $similarApartmentsRepository,
        ApartmentDto $apartmentDto
    ): RedirectResponse
    {
        $manager = $this->getDoctrine()->getManager();
        try {
            $numberApartment = (int)$request->get('numberApartment');
            $similarApartmentsRepository->deleteByApartmentId($numberApartment);
            $apartmentLayoutsRepository->deleteByApartmentId($numberApartment);
            $apartmentData = [
                'typeHouseId'                  => (int)$request->get('typeHouse'),
                'numberApartment'              => $numberApartment,
                'floor'                        => (int)$request->get('floor'),
                'typeApartment'                => $request->get('typeApartment'),
                'quantityRoom'                 => (int)$request->get('quantityRoom', 0),
                'area'                         => (float)$request->get('area', 0),
                'costMeter'                    => (float)$request->get('costMeter', 0),
                'totalCost'                    => (float)$request->get('totalCost', 0),
                'titleApartment'               => $request->get('titleApartment'),
                'apartmentStatus'              => (int)$request->get('apartmentStatus', 0),
                'showInPreview'                => $request->get('showInPreview', null) !== null,
                'placingApartmentOnQuarterMap' => $request->files->get('placingApartmentOnQuarterMap', null),
                'placingApartmentOnFloorPlan'  => $request->files->get('placingApartmentOnFloorPlan', null),
                'selectedSimilarApartments'    => $request->get('selectedSimilarApartments', []),
                'apartmentTypeLayouts'         => $request->get('apartmentTypeLayouts', []),
                'apartmentTypeLayoutsFiles'    => $request->files->get('apartmentTypeLayouts', []),
            ];
            $apartment = $apartmentDto->getObjectFromArray($apartmentData);
            $this->addFlash('notice', sprintf('Добавлена квартира %s', $apartment->getId()));
        } catch (\Throwable $exception) {
            $log = (new Log())->setError($exception->getMessage());
            $manager->persist($log);
            $manager->flush();
        }
        return $this->redirect($this->generateUrl('admin_index'));
    }


    /**
     * @Route("/admin/apartment/edit/show_preview_pdf/{id}/", name="admin_apartment_show_preview_pdf", methods={"GET"}, requirements={"id"="\d+"})
     * @param int $id
     * @param ApartmentRepository $apartmentRepository
     * @param ApartmentDto $apartmentDto
     * @return string|RedirectResponse
     */
    public function showPreviewPdf(int $id, ApartmentRepository $apartmentRepository, ApartmentDto $apartmentDto)
    {
        try {
            $apartment = $apartmentRepository->find($id);
            if ($apartment === null) {
                throw new \LogicException('Не найдена квартира');
            }
            $apartmentData = $apartmentDto->getArrayFromObject($apartment);
            return $this->render('pdf/apartment.html.twig', [
                'apartment' => $apartmentData,
            ]);
        } catch (\Throwable $exception) {
            $manager = $this->getDoctrine()->getManager();
            $log = (new Log())->setError($exception->getMessage());
            $manager->persist($log);
            $manager->flush();
        }
        return $this->redirect($this->generateUrl('admin_index'));
    }
}
