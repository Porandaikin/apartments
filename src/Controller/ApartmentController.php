<?php

namespace App\Controller;

use App\Dto\Emails\RequestCost;
use App\Dto\Response\JsonResponseDto;
use App\Entity\Apartment;
use App\Dto\Entity\Apartment as ApartmentDto;
use App\Repository\ApartmentRepository;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApartmentController extends AbstractController
{
    /**
     * @Route("/apartment/{id}/", name="apartment.show", methods={"GET"})
     * @param int $id
     * @param ApartmentRepository $apartmentRepository
     * @param ApartmentDto $apartmentDto
     * @return Response
     */
    public function show(int $id, ApartmentRepository $apartmentRepository, ApartmentDto $apartmentDto): Response
    {
        /** @var Apartment $apartment */
        $apartment = $apartmentRepository->find($id);
        return $this->render('apartment/index.html.twig', [
            'apartment' => $apartmentDto->getArrayFromObject($apartment),
        ]);
    }

    /**
     * @Route("/apartment/send_request_cost", name="apartment.send_request_cost", methods={"POST"})
     * @param Request $request
     * @param Mailer $mailer
     * @param ApartmentRepository $apartmentRepository
     * @return JsonResponse
     */
    public function sendRequestCost(Request $request, Mailer $mailer, ApartmentRepository $apartmentRepository, ApartmentDto $apartmentDto): JsonResponse
    {
        $status = JsonResponse::HTTP_CREATED;
        $jsonResponseDto = new JsonResponseDto();
        $apartmentId = (int)$request->get('id');
        try {
            /** @var Apartment $apartment */
            $apartment = $apartmentRepository->getApartment($apartmentId);
            if ($apartment === null) {
                throw new \LogicException("Не найдена квартира с id={$apartment->getId()}");
            }
            $requestCostName = $request->get('request_cost_name');
            $requestCostPhone = $request->get('request_cost_phone');
            $requestCostMessage = $request->get('request_cost_message');
            $requestCostDto = (new RequestCost())->setRequestCostName($requestCostName)
                ->setRequestCostPhone($requestCostPhone)
                ->setRequestCostMessage($requestCostMessage)
                ->setApartmentArrayData($apartmentDto->getArrayFromObject($apartment));
            $mailer->sendMail($requestCostDto);
        } catch (\Throwable $exception) {
            $jsonResponseDto->addError($exception->getMessage());
            $status = JsonResponse::HTTP_BAD_REQUEST;
        }

        return $this->json($jsonResponseDto->toArray(), $status);
    }
}
