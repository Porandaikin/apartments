<?php

namespace App\Controller;

use App\Repository\ApartmentRepository;
use App\Repository\HouseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class FilterController extends AbstractController
{
    /**
     * @Route("/filter", name="filter")
     * @param ApartmentRepository $apartmentRepository
     * @param HouseRepository $houseRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ApartmentRepository $apartmentRepository, HouseRepository $houseRepository)
    {
        return $this->render('filter/index.html.twig', [
            'minTotalCost' => $apartmentRepository->getMinTotalCost(),
            'maxTotalCost' => $apartmentRepository->getMaxTotalCost(),
            'maxFloor'     => $apartmentRepository->getMaxFloor(),
            'minArea'      => $apartmentRepository->getMinArea(),
            'maxArea'      => $apartmentRepository->getMaxArea(),
            'houses'       => $houseRepository->findAll(),
        ]);
    }
}
