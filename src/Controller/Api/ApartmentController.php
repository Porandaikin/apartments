<?php

namespace App\Controller\Api;

use App\Config\Apartment\Room;
use App\Config\Apartment\Type;
use App\Dto\Response\JsonResponseDto;
use App\Entity\Apartment;
use App\Repository\ApartmentRepository;
use App\Repository\HouseRepository;
use App\Service\ApartmentPdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ApartmentController extends AbstractController
{
    public const COUNT_ITEMS_ON_PAGE = 1000;

    /**
     * @Route("/api/apartaments", name="api_apartaments_list", methods={"POST", "GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Получить данные о квартирах",
     *     )
     * )
     * @SWG\Parameter(
     *     name="house_type_id",
     *     in="query",
     *     type="string",
     *     description="Идентификатор типа дома",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="floor",
     *     in="query",
     *     type="integer",
     *     description="Этаж",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="type",
     *     in="query",
     *     type="string",
     *     description="Тип картиры",
     *     required=false
     * )
     * @SWG\Parameter(
     *     name="page",
     *     in="query",
     *     type="integer",
     *     description="Номер страницы",
     *     required=false
     * )
     * @SWG\Tag(name="Квартиры")
     * @param Request $request
     * @param ApartmentRepository $apartmentRepository
     * @param UploaderHelper $helper
     * @return JsonResponse
     */
    public function list(Request $request, ApartmentRepository $apartmentRepository, UploaderHelper $helper): JsonResponse
    {
        $jsonResponseDto = new JsonResponseDto();
        $status = JsonResponse::HTTP_OK;
        try {
            $data = $request->getContent();
            $data = json_decode($data, true) ?? [];
            $page = $this->getValue($data, 'page', 1);
            $firstResult = ($page - 1) * self::COUNT_ITEMS_ON_PAGE;
            $type = $this->getValue($data, 'type', null);
            if ($type !== null && $type !== 'all' && !Type::isCorrectType($type)) {
                throw new \InvalidArgumentException('Неправильный тип квартиры');
            }
            $rooms = $this->getValue($data, 'rooms');
            $floors = (int)$this->getValue($data, 'floors');
            $houseTypeId = $this->getValue($data, 'house_type_id');

            /** @var array $apartments */
            $apartments = $apartmentRepository
                ->getFilterData([
                    'houses_type_id' => $houseTypeId !== null && $houseTypeId !== 0 ? [$houseTypeId] : [],
                    'floors'         => $floors !== null && $floors !== 0 ? [$floors] : [],
                    'type'           => $type,
                ], true);

            $apartmentsData = array_map(static function (Apartment $apartment) use ($helper) {
                return [
                    'id'                              => $apartment->getId(),
                    'title'                           => $apartment->getTitle(),
                    'houseType'                       => $apartment->getHouse() !== null ? $apartment->getHouse()->getType() : null,
                    'floor'                           => $apartment->getFloor(),
                    'totalCost'                       => $apartment->getTotalCostString(),
                    'costMeter'                       => $apartment->getCostMeterString(),
                    'showInPreview'                   => $apartment->getShowInPreview(),
                    'type'                            => $apartment->getType(),
                    'status'                          => $apartment->getStatus(),
                    'area'                            => $apartment->getArea(),
                    'rooms'                           => Room::getNameByQuantityRoom($apartment->getQuantityRoom()),
                    'main_layout'                     => self::getMainLayout($apartment, $helper),
                    'placingApartmentOnFloorPlanFile' => $helper->asset($apartment, 'placingApartmentOnFloorPlanFile'),
                ];
            }, $apartments);
            $jsonResponseDto->setData($apartmentsData);
        } catch (\Throwable $exception) {
            $jsonResponseDto->addError($exception->getMessage());
            $status = JsonResponse::HTTP_BAD_REQUEST;
        }

        return $this->json($jsonResponseDto->toArray(), $status);
    }

    /**
     * @Route("/api/apartament/{id}/", name="api_apartament_edit", methods={"GET"})
     * @param int $id
     * @param ApartmentRepository $apartmentRepository
     * @param UploaderHelper $helper
     * @return JsonResponse
     */
    public function getDataApartment(int $id, ApartmentRepository $apartmentRepository, UploaderHelper $helper, ApartmentPdf $apartmentPdf): JsonResponse
    {
        /** @var Apartment $apartment */
        $apartment = $apartmentRepository->find($id);
        if ($apartment === null) {
            $apartment = new Apartment();
        }
        $apartmentData = [
            'selectedHouseId'              => $apartment->getHouse() ? $apartment->getHouse()->getId() : '',
            'floor'                        => $apartment->getFloor(),
            'selectedType'                 => $apartment->getType(),
            'selectedRoom'                 => $apartment->getQuantityRoom(),
            'area'                         => $apartment->getArea(),
            'costMeter'                    => $apartment->getCostMeter(),
            'totalCost'                    => $apartment->getTotalCost(),
            'status'                       => $apartment->getStatus(),
            'title'                        => $apartment->getTitle(),
            'apartmentStatus'              => $apartment->getStatus(),
            'showInPreview'                => $apartment->getShowInPreview(),
            'placingApartmentOnQuarterMap' => $helper->asset($apartment, 'placingApartmentOnQuarterMapFile'),
            'placingApartmentOnFloorPlan'  => $helper->asset($apartment, 'placingApartmentOnFloorPlanFile'),
            'pathToPdf'                    => $apartment->getPathToPdf(),

        ];

        return $this->json($apartmentData);
    }

    /**
     * @Route("/api/getApartmentsByFilter", name="api_apartament_getApartmentsByFilter", methods={"GET"})
     * @param Request $request
     * @param ApartmentRepository $apartmentRepository
     * @param UploaderHelper $helper
     * @return JsonResponse
     */
    public function getApartmentsByFilter(Request $request, ApartmentRepository $apartmentRepository, UploaderHelper $helper): JsonResponse
    {
        $jsonResponseDto = new JsonResponseDto();
        $status = JsonResponse::HTTP_OK;
        try {
            $data = [];
            foreach ($request->query->all() as $key => $value) {
                $key = str_replace('?', '', $key);
                $data[$key] = $value;
            }

            /** @var array $apartments */
            $apartments = $apartmentRepository
                ->getFilterData([
                    'houses_type_id' => $this->getValue($data, 'house_type_id', 'all'),
                    'rooms'          => $this->getValue($data, 'rooms', []),
                    'floors'         => $this->getValue($data, 'floors', []),
                    'price_from'     => $this->getValue($data, 'price_from'),
                    'type'           => $this->getValue($data, 'type'),
                    'price_to'       => $this->getValue($data, 'price_to'),
                    'area_from'      => $this->getValue($data, 'area_from'),
                    'area_to'        => $this->getValue($data, 'area_to'),
                ], true);

            $apartmentsData = array_map(static function (Apartment $apartment) use ($helper) {
                return [
                    'id'                              => $apartment->getId(),
                    'title'                           => $apartment->getTitle(),
                    'houseType'                       => $apartment->getHouse() !== null ? $apartment->getHouse()->getType() : null,
                    'floor'                           => $apartment->getFloor(),
                    'totalCost'                       => $apartment->getTotalCostString(),
                    'costMeter'                       => $apartment->getCostMeterString(),
                    'showInPreview'                   => $apartment->getShowInPreview(),
                    'type'                            => $apartment->getType(),
                    'status'                          => $apartment->getStatus(),
                    'area'                            => $apartment->getArea(),
                    'rooms'                           => Room::getNameByQuantityRoom($apartment->getQuantityRoom()),
                    'main_layout'                     => self::getMainLayout($apartment, $helper),
                    'placingApartmentOnFloorPlanFile' => $helper->asset($apartment, 'placingApartmentOnFloorPlanFile'),
                ];
            }, $apartments);
            $jsonResponseDto->setData($apartmentsData);
        } catch (\Throwable $exception) {
            $jsonResponseDto->addError($exception->getMessage());
            $status = JsonResponse::HTTP_BAD_REQUEST;
        }

        return $this->json($apartmentsData, $status);
    }

    /**
     * @param Apartment $apartment
     * @param UploaderHelper $helper
     * @return string|null
     */
    public static function getMainLayout(Apartment $apartment, UploaderHelper $helper): ?string
    {
        $mainLayout = '';
        foreach ($apartment->getApartmentLayouts() as $layout) {
            if ($layout->getType() === \App\Config\ApartmentLayout\Type::MAIN_INT) {
                $mainLayout = $helper->asset($layout, 'pathToFile');
                break;
            }
        }

        return $mainLayout;
    }

    /**
     * @Route("/api/get_tooltip_house/{type}/", name="api_apartament_get_tooltip_house", methods={"GET"})
     * @param string $type
     * @param ApartmentRepository $apartmentRepository
     * @param HouseRepository $houseRepository
     * @return JsonResponse
     */
    public function getTooltip(string $type, ApartmentRepository $apartmentRepository, HouseRepository $houseRepository): JsonResponse
    {
        $result = [
            'type'      => $type,
            'is_vacant' => false, // Квартиры в наличии
            'roomsList' => [],
        ];
        $house = $houseRepository->getByType($type);
        if ($house !== null) {
            $roomsList = [];
            $roomsListData = $apartmentRepository->getDataForPreviewByHouse($house);
            if (count($roomsListData) > 0) {
                $result['is_vacant'] = true;
                foreach ($roomsListData as $roomData) {
                    $roomsList[] = [
                        'caption'   => ($roomData['quantityRoom'] > 0) ? $roomData['quantityRoom'] . '-комнатные' : Room::PENTHOUSE,
                        'rooms'     => $roomData['quantityRoom'],
                        'min_price' => $roomData['minTotalCost'],
                        'link'      => $this->getParameter('app.url_app') . "/filter?house_type_id%5B%5D={$house->getId()}&rooms%5B%5D={$roomData['quantityRoom']}"
                    ];
                }
                $result['roomsList'] = $roomsList;
            }
        }
        return $this->json($result);
    }

    /**
     * @param array $array
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    private function getValue(array $array, $key, $default = null)
    {
        return $array[$key] ?? $default;
    }
}
