<?php

namespace App\Controller\Api;

use App\Dto\Response\JsonResponseDto;
use App\Repository\HouseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HouseController extends AbstractController
{
    /**
     * @Route("/api/house/list", name="api_house_list", methods={"POST"})
     * @param HouseRepository $houseRepository
     * @return Response
     */
    public function list(HouseRepository $houseRepository)
    {
        $jsonResponseDto = new JsonResponseDto();
        $status = JsonResponse::HTTP_OK;
        try {
            $jsonResponseDto->setData($houseRepository->getDataAllHouses());
        } catch (\Throwable $exception) {
            $jsonResponseDto->addError($exception->getMessage());
            $status = JsonResponse::HTTP_BAD_REQUEST;
        }
        return $this->json($jsonResponseDto->toArray(), $status);
    }
}
