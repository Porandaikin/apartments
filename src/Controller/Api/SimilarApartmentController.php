<?php

namespace App\Controller\Api;

use App\Repository\ApartmentRepository;
use App\Repository\SimilarApartmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SimilarApartmentController extends AbstractController
{
    /**
     * @Route("/api/similar/getSimilarApartments/{id}/", name="api_similar_getSimilarApartments")
     * @param $id
     * @param SimilarApartmentRepository $similarApartmentsRepository
     * @return JsonResponse
     */
    public function getSimilarApartments($id, SimilarApartmentRepository $similarApartmentsRepository): JsonResponse
    {
        return $this->json($similarApartmentsRepository->getSimilarApartmentById($id));
    }

    /**
     * @Route("/api/similar/getNotSimilarApartments/{id}/", name="api_similar_getNotSimilarApartments")
     * @param $id
     * @param SimilarApartmentRepository $similarApartmentsRepository
     * @param ApartmentRepository $apartmentRepository
     * @return JsonResponse
     */
    public function getNotSimilarApartments(
        $id,
        SimilarApartmentRepository $similarApartmentsRepository,
        ApartmentRepository $apartmentRepository
    ): JsonResponse
    {
        $similarApartments = $similarApartmentsRepository->getSimilarApartmentById($id);
        $notSimilarApartments = $apartmentRepository->getEverythingExcept(array_column($similarApartments, 'id'));
        return $this->json($notSimilarApartments);
    }
}
