<?php

namespace App\Controller\Api;

use App\Config\ApartmentLayout\Type;
use App\Entity\ApartmentLayouts;
use App\Repository\ApartmentLayoutsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class ApartmentLayoutController extends AbstractController
{
    /**
     * @Route("/api/apartment/getLayout/{id}/", name="api_apartment_getLayout")
     * @param int $id
     * @param ApartmentLayoutsRepository $apartmentLayoutsRepository
     * @param UploaderHelper $helper
     * @return JsonResponse
     */
    public function getLayout(int $id, ApartmentLayoutsRepository $apartmentLayoutsRepository, UploaderHelper $helper): JsonResponse
    {
        $resultData = [];
        $layouts = $apartmentLayoutsRepository->getLayoutByApartmentId($id);
        /** @var ApartmentLayouts $layout */
        foreach ($layouts as $layout) {
            $resultData[] = [
                'id'           => $layout->getId(),
                'type_id'      => $layout->getType(),
                'type_name'    => Type::getNameByInt($layout->getType()),
                'path_to_file' => $helper->asset($layout, 'pathToFile'),
                'path'         => $layout->getPath(),
            ];
        }

        return $this->json($resultData);
    }
}
