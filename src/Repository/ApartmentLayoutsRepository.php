<?php

namespace App\Repository;

use App\Entity\ApartmentLayouts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ApartmentLayouts|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApartmentLayouts|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApartmentLayouts[]    findAll()
 * @method ApartmentLayouts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApartmentLayoutsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ApartmentLayouts::class);
    }

    // /**
    //  * @return ApartmentLayouts[] Returns an array of ApartmentLayouts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function getLayoutByApartmentId(int $apartmentId)
    {
        return $this->createQueryBuilder('apartment_layout')
            ->andWhere('apartment_layout.apartment = :apartmentId')
            ->setParameter('apartmentId', $apartmentId)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $apartmentId
     */
    public function deleteByApartmentId(int $apartmentId): void
    {
        $this->createQueryBuilder('apartment_layout')
            ->delete()
            ->where('apartment_layout.apartment = :apartmentId')
            ->setParameter('apartmentId', $apartmentId)
            ->getQuery()
            ->execute();
    }

}
