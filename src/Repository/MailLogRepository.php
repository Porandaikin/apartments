<?php

namespace App\Repository;

use App\Entity\MailLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MailLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailLog[]    findAll()
 * @method MailLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailLogRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MailLog::class);
    }

    // /**
    //  * @return MailLog[] Returns an array of MailLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MailLog
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
