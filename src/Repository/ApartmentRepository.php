<?php

namespace App\Repository;

use App\Config\Apartment\Status;
use App\Config\Apartment\Type;
use App\Entity\Apartment;
use App\Entity\House;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Apartment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Apartment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Apartment[]    findAll()
 * @method Apartment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApartmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Apartment::class);
    }

    /**
     * @param array $filter
     * @param bool $onlyAllowedStatuses
     * @return array
     */
    public function getFilterData($filter = [], bool $onlyAllowedStatuses = false): array
    {
        $queryBuilder = $this->createQueryBuilder('apartment')
            ->leftJoin('apartment.house', 'house');
        if (count($filter) > 0) {
            if (array_key_exists('houses_type_id', $filter) && count($filter['houses_type_id']) > 0 && $filter['houses_type_id'] !== 'all') {
                $housesTypeId = array_map(static function ($houseTypeId) {
                    return (int)$houseTypeId;
                }, $filter['houses_type_id']);
                $queryBuilder->andWhere('house.id IN (:houses_id)')
                    ->setParameter('houses_id', $housesTypeId);
            }
            if (array_key_exists('rooms', $filter) && count($filter['rooms']) > 0) {
                $rooms = array_map(static function ($room) {
                    return (int)$room;
                }, $filter['rooms']);
                $queryBuilder->andWhere('apartment.quantityRoom IN (:quantityRooms)')
                    ->setParameter('quantityRooms', $rooms);
            }
            if (array_key_exists('floors', $filter) && count($filter['floors']) > 0) {
                $floors = array_map(static function ($floor) {
                    return (int)$floor;
                }, $filter['floors']);
                $queryBuilder->andWhere('apartment.floor IN (:floors)')
                    ->setParameter('floors', $floors);
            }
            if (array_key_exists('price_from', $filter) && $filter['price_from'] !== null) {
                $priceFrom = (int)$filter['price_from'];
                $queryBuilder->andWhere('apartment.totalCost >= :price_from')
                    ->setParameter('price_from', $priceFrom);
            }
            if (array_key_exists('price_from', $filter) && $filter['price_to'] !== null) {
                $priceTo = (int)$filter['price_to'];
                $queryBuilder->andWhere('apartment.totalCost <= :price_to')
                    ->setParameter('price_to', $priceTo);
            }
            if (array_key_exists('area_from', $filter) && $filter['area_from'] !== null) {
                $areaFrom = (int)$filter['area_from'];
                $queryBuilder->andWhere('apartment.area >= :area_from')
                    ->setParameter('area_from', $areaFrom);
            }
            if (array_key_exists('area_to', $filter) && $filter['area_to'] !== null) {
                $areaTo = (int)$filter['area_to'];
                $queryBuilder->andWhere('apartment.area <= :area_to')
                    ->setParameter('area_to', $areaTo);
            }
            if (array_key_exists('type', $filter) && $filter['type'] !== null && $filter['type'] !== 'all' && Type::isCorrectType($filter['type'])) {
                $queryBuilder->andWhere('apartment.type <= :type')
                    ->setParameter('type', $filter['type']);
            }
        }
        if ($onlyAllowedStatuses) {
            $queryBuilder->andWhere('apartment.status IN (:allowedStatusesIds)')
                ->setParameter('allowedStatusesIds', Status::getAllowedStatusesIds());
        }
        $queryBuilder->orderBy(' apartment.id');

        return $queryBuilder->getQuery()
            ->getResult();
    }

    public function getApartment(int $id): ?Apartment
    {
        return $this->find($id);
    }

    /**
     * @return mixed
     */
    public function getMaxId(): int
    {
        $resultQuery = $this->createQueryBuilder('apartment')
            ->select(['apartment.id'])
            ->orderBy('apartment.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
        return count($resultQuery) > 0 ? $resultQuery[0]['id'] : 0;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getEverythingExcept(array $ids): array
    {
        $queryBuilder = $this->createQueryBuilder('apartments')
            ->select(['apartments.id, apartments.title']);
        if (count($ids) > 0) {
            $queryBuilder->andWhere('apartments.id NOT IN (:ids)')
                ->setParameter('ids', $ids);
        }
        return $queryBuilder->getQuery()
            ->getArrayResult();
    }

    public function getMaxArea(): int
    {
        $resultQuery = $this->createQueryBuilder('apartment')
            ->select(['apartment.area'])
            ->orderBy('apartment.area', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($resultQuery) > 0 ? $resultQuery[0]['area'] : 0;
    }

    public function getMinArea(): int
    {
        $resultQuery = $this->createQueryBuilder('apartment')
            ->select(['apartment.area'])
            ->orderBy('apartment.area', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($resultQuery) > 0 ? $resultQuery[0]['area'] : 0;
    }

    public function getMinTotalCost(): int
    {
        $resultQuery = $this->createQueryBuilder('apartment')
            ->select(['apartment.totalCost'])
            ->orderBy('apartment.totalCost', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($resultQuery) > 0 ? $resultQuery[0]['totalCost'] : 0;
    }

    public function getMaxTotalCost(): int
    {
        $resultQuery = $this->createQueryBuilder('apartment')
            ->select(['apartment.totalCost'])
            ->orderBy('apartment.totalCost', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($resultQuery) > 0 ? $resultQuery[0]['totalCost'] : 0;
    }

    /**
     * @param House $house
     * @return array
     */
    public function getDataForPreviewByHouse(House $house): array
    {
        $resultQuery = $this->createQueryBuilder('apartment')
            ->select('apartment.quantityRoom', 'min(apartment.totalCost) as minTotalCost')
            ->andWhere('apartment.showInPreview = :showInPreview')
            ->andWhere('apartment.house = :house')
            ->andWhere('apartment.status = :status')
            ->setParameters([
                'showInPreview' => true,
                'house'         => $house,
                'status'        => Status::FREE_INT,
            ])
            ->groupBy('apartment.quantityRoom');

        return $resultQuery->getQuery()->getArrayResult();
    }

    /**
     * @return int
     */
    public function getMaxFloor(): int
    {
        $resultQuery = $this->createQueryBuilder('apartment')
            ->select(['apartment.floor'])
            ->orderBy('apartment.floor', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return count($resultQuery) > 0 ? $resultQuery[0]['floor'] : 0;
    }
}
