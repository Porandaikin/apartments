<?php

namespace App\Repository;

use App\Entity\House;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method House|null find($id, $lockMode = null, $lockVersion = null)
 * @method House|null findOneBy(array $criteria, array $orderBy = null)
 * @method House[]    findAll()
 * @method House[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HouseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, House::class);
    }

    // /**
    //  * @return House[] Returns an array of House objects
    //  */

    public function getDataAllHouses()
    {
        return $this->createQueryBuilder('house', 'house.id')
            ->orderBy('house.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getArrayResult()
        ;
    }

    /**
     * @param string $type
     * @return House|null
     */
    public function getByType(string $type): ?House
    {
        return $this->findOneBy(['type' => $type]);
    }
}
