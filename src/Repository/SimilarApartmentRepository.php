<?php

namespace App\Repository;

use App\Entity\SimilarApartment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SimilarApartment|null find($id, $lockMode = null, $lockVersion = null)
 * @method SimilarApartment|null findOneBy(array $criteria, array $orderBy = null)
 * @method SimilarApartment[]    findAll()
 * @method SimilarApartment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SimilarApartmentRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SimilarApartment::class);
    }

    // /**
    //  * @return SimilarApartments[] Returns an array of SimilarApartments objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /**
     * @param int $id
     * @return array
     */
    public function getSimilarApartmentById(int $id): array
    {
        return  $this->createQueryBuilder('similar_apartment')
            ->select(['apartments.id, apartments.title'])
            ->andWhere('similar_apartment.parentApartment = :id')
            ->join('similar_apartment.similarApartment', 'apartments')
            ->setParameter('id', $id)
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param int $apartmentId
     */
    public function deleteByApartmentId(int $apartmentId): void
    {
        $this->createQueryBuilder('similar_apartment')
            ->delete()
            ->where('similar_apartment.parentApartment = :apartmentId')
            ->setParameter('apartmentId', $apartmentId)
            ->getQuery()
            ->execute();
    }
}
