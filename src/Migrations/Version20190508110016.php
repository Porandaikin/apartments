<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190508110016 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("
            CREATE TABLE logs (
                    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    error VARCHAR(1000) NOT NULL DEFAULT '' COMMENT 'Содержание ошибки',                        
                    created_at  DATETIME NOT NULL COMMENT 'Время создания',   
                    updated_at  DATETIME NOT NULL COMMENT 'Время обновления'      
            )
        ");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE logs');

    }
}
