<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190420175129 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Добавление таблицы планировки квартиры';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE apartment_layouts(
                id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                apartment_id INT UNSIGNED NOT NULL COMMENT 'идентификатор квартиры',
                path_to_file VARCHAR(250) NOT NULL COMMENT 'путь до планировки квартиры',
                type INT NULL COMMENT 'тип планировки в числовом представлении, описание в конфиге',
                created_at  DATETIME NOT NULL COMMENT 'Время создания',   
                updated_at  DATETIME NOT NULL COMMENT 'Время обновления', 
                FOREIGN KEY (apartment_id) REFERENCES apartments(id) ON DELETE CASCADE 
             )
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('
            DROP TABLE apartment_layouts;
        ');
    }
}
