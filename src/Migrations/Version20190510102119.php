<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190510102119 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Добавить получателей и отправителей в таблицу mail_log';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE mail_log
                ADD COLUMN send_to VARCHAR(250) NULL COMMENT 'Кому отправляется письмо' AFTER id,
                ADD COLUMN send_cc VARCHAR(250) NULL COMMENT 'Кому отправляется копия письма' AFTER send_to;
        ");

    }

    public function down(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE mail_log
                DROP COLUMN send_to,
                DROP COLUMN send_cc
        ');
    }
}
