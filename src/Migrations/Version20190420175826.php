<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190420175826 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Добавление таблицы похожие квартиры';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("
            CREATE TABLE similar_apartments(                
                id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                parent_id INT UNSIGNED NOT NULL COMMENT 'идентификатор родительской квартиры',
                similar_apartment_id INT UNSIGNED NOT NULL COMMENT 'идентификатор похожеё квартиры',
                FOREIGN KEY (parent_id) REFERENCES apartments(id) ON DELETE CASCADE,
                FOREIGN KEY (similar_apartment_id) REFERENCES apartments(id) ON DELETE CASCADE            
            );
        ");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("
            DROP TABLE similar_apartments;
        ");
    }
}
