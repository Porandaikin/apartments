<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190508093535 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Создать таблицу логов mail';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE mail_log (
                    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    message VARCHAR(1000) NOT NULL DEFAULT '' COMMENT 'Содержание письма',
                    result_mail INT(12) NULL COMMENT 'Результат отправки письма',                            
                    created_at  DATETIME NOT NULL COMMENT 'Время создания',   
                    updated_at  DATETIME NOT NULL COMMENT 'Время обновления'      
            )
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE mail_log');
    }
}
