<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190420120557 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Создать таблицу house';
    }

    public function up(Schema $schema) : void
    {
       $this->addSql("
            CREATE TABLE houses (
                id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                type VARCHAR(50) NOT NULL COMMENT 'Тип квартиры',
                quantity_floor  INT(5) NOT NULL DEFAULT 1 COMMENT 'количество этажей в доме'              
            );
       ");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE houses;');
    }
}
