<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190420122208 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Создать таблицу apartments';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE apartments (
                    id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
                    title VARCHAR(50) NOT NULL COMMENT 'заголовок квартиры',
                    area DOUBLE(10,2) NOT NULL COMMENT 'площадь',
                    quantity_room INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'кол-во комнат', 
                    type ENUM('roomed', 'penthouse') COMMENT 'Тип картиры: пентхаус, комнатная',
                    floor INT UNSIGNED NOT NULL DEFAULT 1 COMMENT 'этаж',
                    cost_meter DOUBLE(10,2) COMMENT 'стоимость за кв. метр',
                    total_cost DOUBLE(10,2) COMMENT 'стоимость общая',
                    placing_apartment_on_quarter_map VARCHAR(250) NULL COMMENT 'размещением квартиры на схеме квартала (чтобы понимать, куда выходят окна)',
                    placing_apartment_on_floor_plan VARCHAR(250) NULL COMMENT 'размещением квартиры на схеме этажа',
                    show_in_preview TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'показывать в препросмотре?',    
                    path_to_pdf VARCHAR(250) NULL COMMENT 'путь до pdf файла',
                    status INT(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'статус квартиры в числовом предствлении, описание смотреть в конфиге',
                    house_id INT UNSIGNED NULL COMMENT 'Идентификатор дома',            
                    created_at  DATETIME NOT NULL COMMENT 'Время создания',   
                    updated_at  DATETIME NOT NULL COMMENT 'Время обновления',   
                    FOREIGN KEY (house_id)  REFERENCES houses (id) ON DELETE SET NULL
                );
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('
            DROP TABLE apartments;
        ');
    }
}
