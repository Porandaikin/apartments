<?php


namespace App\Dto\Response;

use Doctrine\Common\Collections\ArrayCollection;

class JsonResponseDto
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var string
     */
    private $status = true;

    /**
     * @var array
     */
    private $errors;

    /**
     * JsonResponseDto constructor.
     */
    public function __construct()
    {
        $this->errors = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getErrorsString(): ?string
    {
        return implode(';', $this->errors->getValues());
    }

    /**
     * @param string $error
     * @return JsonResponseDto
     */
    public function addError(string $error): self
    {
        $this->errors->add($error);
        $this->setStatus(false);

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'data' => $this->getData(),
            'status' => $this->getStatus(),
            'errors' => $this->getErrorsString(),
        ];
    }
}
