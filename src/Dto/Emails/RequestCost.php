<?php

namespace App\Dto\Emails;

class RequestCost
{
    /** @var string */
    private $requestCostName;

    /** @var string */
    private $requestCostPhone;

    /** @var string */
    private $requestCostMessage;

    /** @var array */
    private $apartmentArrayData;

    /**
     * @return string
     */
    public function getRequestCostName(): string
    {
        return $this->requestCostName;
    }

    /**
     * @param string $requestCostName
     * @return RequestCost
     */
    public function setRequestCostName(string $requestCostName): self
    {
        $this->requestCostName = $requestCostName;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestCostPhone(): string
    {
        return $this->requestCostPhone;
    }

    /**
     * @param string $requestCostPhone
     * @return RequestCost
     */
    public function setRequestCostPhone(string $requestCostPhone): self
    {
        $this->requestCostPhone = $requestCostPhone;

        return $this;
    }

    /**
     * @return array
     */
    public function getApartmentArrayData(): array
    {
        return $this->apartmentArrayData;
    }

    /**
     * @param array $apartmentArrayData
     * @return RequestCost
     */
    public function setApartmentArrayData(array $apartmentArrayData): self
    {
        $this->apartmentArrayData = $apartmentArrayData;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestCostMessage(): string
    {
        return $this->requestCostMessage;
    }

    /**
     * @param string $requestCostMessage
     * @return RequestCost
     */
    public function setRequestCostMessage(string $requestCostMessage): self
    {
        $this->requestCostMessage = $requestCostMessage;

        return $this;
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'requestCostName'    => $this->getRequestCostName(),
            'requestCostPhone'   => $this->getRequestCostPhone(),
            'requestCostMessage' => $this->getRequestCostMessage(),
            'apartment'          => $this->getApartmentArrayData(),
        ];
    }
}
