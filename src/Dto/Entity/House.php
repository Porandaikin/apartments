<?php


namespace App\Dto\Entity;

use \App\Entity\House as HouseEntity;

class House
{
    public function getArrayFromObject(HouseEntity $house): array
    {
        return [
            'id'            => $house->getId(),
            'type'          => $house->getType(),
            'quantityFloor' => $house->getQuantityFloor(),
        ];
    }
}