<?php

namespace App\Dto\Entity;

use App\Repository\ApartmentRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\ApartmentLayouts as ApartmentLayoutsEntity;
use Symfony\Component\HttpFoundation\File\File;

class ApartmentLayouts
{
    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(ApartmentRepository $apartmentRepository, EntityManagerInterface $entityManager)
    {
        $this->apartmentRepository = $apartmentRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $apartmentLayoutsData
     * @return ApartmentLayoutsEntity|null
     * @throws \Exception
     */
    public function getObjectFromArray(array $apartmentLayoutsData): ?ApartmentLayoutsEntity
    {
        /** @var \App\Entity\Apartment $apartment */
        $apartment = $this->apartmentRepository->find($apartmentLayoutsData['apartmentId']);
        $typeId = (int)$apartmentLayoutsData['typeId'];
        $path = $apartmentLayoutsData['path'];
        $pathToFile = $apartmentLayoutsData['pathToFile'];
        if ($apartment !== null && $typeId && (!empty($path) || $pathToFile instanceof File)) {
            $apartmentLayout = new ApartmentLayoutsEntity();

            if (!empty($path)) {
                $apartmentLayout->setPath($path);
            } else {
                $apartmentLayout->setPathToFile($pathToFile);
            }
            $apartmentLayout->setApartment($apartment)
                ->setType($typeId);
            $this->entityManager->persist($apartmentLayout);
            $this->entityManager->flush();

            return $apartmentLayout;
        }

        return null;
    }
}