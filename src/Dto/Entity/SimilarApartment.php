<?php

namespace App\Dto\Entity;

use App\Repository\ApartmentRepository;
use \App\Entity\SimilarApartment as SimilarApartmentEntity;
use Doctrine\ORM\EntityManagerInterface;

class SimilarApartment
{
    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(ApartmentRepository $apartmentRepository, EntityManagerInterface $entityManager)
    {
        $this->apartmentRepository = $apartmentRepository;
        $this->entityManager = $entityManager;
    }

    public function getObjectFromArray(array $similarApartmentData): ?SimilarApartmentEntity
    {
        $childApartment = $this->apartmentRepository->find($similarApartmentData['childApartmentId']);
        $parentApartment = $this->apartmentRepository->find($similarApartmentData['parentApartmentId']);
        if ($childApartment !== null && $parentApartment !== null) {
            $similarApartment = new SimilarApartmentEntity();
            $similarApartment->setParentApartment($parentApartment)
                ->setSimilarApartment($childApartment);
            $this->entityManager->persist($similarApartment);
            $this->entityManager->flush();

            return $similarApartment;
        }

        return null;
    }
}
