<?php


namespace App\Dto\Entity;

use App\Config\ApartmentLayout\Type;
use App\Dto\Entity\ApartmentLayouts as ApartmentLayoutsDto;
use App\Dto\Entity\House as HouseDto;
use App\Dto\Entity\SimilarApartment as SimilarApartmentDto;
use App\Entity\Apartment as ApartmentEntity;
use App\Entity\ApartmentLayouts;
use App\Entity\House;
use App\Entity\SimilarApartment;
use App\Repository\ApartmentRepository;
use App\Repository\HouseRepository;
use App\Service\ApartmentPdf;
use Doctrine\ORM\EntityManagerInterface;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class Apartment
{
    /**
     * @var UploaderHelper
     */
    private $helper;

    /**
     * @var HouseRepository
     */
    private $houseRepository;

    /**
     * @var ApartmentRepository
     */
    private $apartmentRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \App\Dto\Entity\SimilarApartment
     */
    private $similarApartmentDto;

    /**
     * @var \App\Dto\Entity\ApartmentLayouts
     */
    private $apartmentLayoutsDto;

    /**
     * @var \App\Dto\Entity\House
     */
    private $houseDto;

    /**
     * @var ApartmentPdf
     */
    private $apartmentPdf;

    public function __construct(
        UploaderHelper $helper,
        HouseRepository $houseRepository,
        ApartmentRepository $apartmentRepository,
        EntityManagerInterface $entityManager,
        SimilarApartmentDto $similarApartmentDto,
        ApartmentLayoutsDto $apartmentLayoutsDto,
        HouseDto $houseDto,
        ApartmentPdf $apartmentPdf
    )
    {
        $this->helper = $helper;
        $this->houseRepository = $houseRepository;
        $this->apartmentRepository = $apartmentRepository;
        $this->entityManager = $entityManager;
        $this->similarApartmentDto = $similarApartmentDto;
        $this->apartmentLayoutsDto = $apartmentLayoutsDto;
        $this->houseDto = $houseDto;
        $this->apartmentPdf = $apartmentPdf;
    }

    /**
     * @param ApartmentEntity $apartment
     * @return array
     */
    public function getArrayFromObject(ApartmentEntity $apartment): array
    {
        return [
            'id'                               => $apartment->getId(),
            'title'                            => $apartment->getTitle(),
            'area'                             => $apartment->getArea(),
            'rooms'                            => $apartment->getQuantityRoom(),
            'type'                             => $apartment->getType(),
            'floor'                            => $apartment->getFloor(),
            'costMeter'                        => $apartment->getCostMeter(),
            'totalCost'                        => $apartment->getTotalCost(),
            'placingApartmentOnQuarterMap'     => $apartment->getPlacingApartmentOnQuarterMap(),
            'placingApartmentOnQuarterMapFile' => $this->helper->asset($apartment, 'placingApartmentOnQuarterMapFile'),
            'placingApartmentOnFloorPlan'      => $apartment->getPlacingApartmentOnFloorPlan(),
            'placingApartmentOnFloorPlanFile'  => $this->helper->asset($apartment, 'placingApartmentOnFloorPlanFile'),
            'showInPreview'                    => (int)$apartment->getShowInPreview(),
            'pathToPdf'                        => $apartment->getPathToPdf(),
            'status'                           => $apartment->getStatus(),
            'createdAt'                        => $apartment->getCreatedAt(),
            'updatedAt'                        => $apartment->getUpdatedAt(),
            'house'                            => $apartment->getHouse() !== null
                ? $this->houseDto->getArrayFromObject($apartment->getHouse())
                : null,
            'similarApartments'                => $this->getSimilarApartments($apartment),
            'layouts'                          => $this->getLayouts($apartment),
        ];
    }

    /**
     * @param ApartmentEntity $apartment
     * @return array
     */
    public function getSimilarApartments(ApartmentEntity $apartment): array
    {
        $resultArray = [];
        /** @var SimilarApartment $similarApartment */
        foreach ($apartment->getSimilarApartments() as $similarApartment) {
            $resultArray[] = $this->getArrayFromObject($similarApartment->getSimilarApartment());
        }
        return $resultArray;
    }

    /**
     * @param ApartmentEntity $apartment
     * @return array
     */
    public function getLayouts(ApartmentEntity $apartment): array
    {
        $layouts = [
            'mainLayout'      => '',
            'furnitureLayout' => '',
            'render3DLayout'  => '',
            'otherLayout'     => '',
        ];
        if ($apartment !== null) {
            /** @var ApartmentLayouts $layout */
            foreach ($apartment->getApartmentLayouts() as $layout) {
                switch ($layout->getType()) {
                    case Type::MAIN_INT :
                        $layouts['mainLayout'] = $this->helper->asset($layout, 'pathToFile');
                        break;
                    case Type::WITH_FURNITURE_ID :
                        $layouts['furnitureLayout'] = $this->helper->asset($layout, 'pathToFile');
                        break;
                    case Type::RENDER_3D_ID :
                        $layouts['render3DLayout'] = $this->helper->asset($layout, 'pathToFile');
                        break;
                    case Type::OTHER_INT :
                        $layouts['otherLayout'] = $this->helper->asset($layout, 'pathToFile');
                }
            }
        }

        return $layouts;
    }

    /**
     * @param array $apartmentData
     * @return ApartmentEntity
     * @throws \Exception
     */
    public function getObjectFromArray(array $apartmentData): ApartmentEntity
    {
        /** @var House $house */
        $house = $this->houseRepository->find($apartmentData['typeHouseId']);
        $apartment = $this->apartmentRepository->find($apartmentData['numberApartment']) ?? new ApartmentEntity();

        if ($apartment->getId() === null) {
            $apartment->setId($apartmentData['numberApartment']);
            $this->entityManager->persist($apartment);
        }

        $apartment
            ->setHouse($house)
            ->setFloor($apartmentData['floor'])
            ->setType($apartmentData['typeApartment'])
            ->setQuantityRoom($apartmentData['quantityRoom'])
            ->setArea($apartmentData['area'])
            ->setCostMeter($apartmentData['costMeter'])
            ->setTotalCost($apartmentData['totalCost'])
            ->setTitle($apartmentData['titleApartment'])
            ->setStatus($apartmentData['apartmentStatus'])
            ->setShowInPreview($apartmentData['showInPreview'])
            ->setPlacingApartmentOnQuarterMapFile($apartmentData['placingApartmentOnQuarterMap'])
            ->setPlacingApartmentOnFloorPlanFile($apartmentData['placingApartmentOnFloorPlan']);

        $this->entityManager->flush();

        foreach ($apartmentData['selectedSimilarApartments'] as $selectedSimilarApartmentId) {
            $similarApartment = $this->similarApartmentDto->getObjectFromArray([
                'childApartmentId'  => $selectedSimilarApartmentId,
                'parentApartmentId' => $apartment->getId(),
            ]);
            if ($similarApartment !== null) {
                $apartment->addSimilarApartments($similarApartment);
            }

        }
        $apartmentTypeLayoutsFiles = $apartmentData['apartmentTypeLayoutsFiles'];
        foreach ($apartmentData['apartmentTypeLayouts'] as $apartmentTypeLayoutIndex => $apartmentTypeLayoutValue) {
            $apartmentLayout = $this->apartmentLayoutsDto->getObjectFromArray([
                'apartmentId' => $apartment->getId(),
                'path'        => $apartmentTypeLayoutValue['path'] ?? '',
                'pathToFile'  => $apartmentTypeLayoutsFiles[$apartmentTypeLayoutIndex]['pathToFile'] ?? '',
                'typeId'      => $apartmentTypeLayoutValue['typeId'],

            ]);
            if ($apartmentLayout !== null) {
                $apartment->addApartmentLayouts($apartmentLayout);
            }
        }

        $apartment->setPathToPdf($this->apartmentPdf->getPdf($this->getArrayFromObject($apartment)));
        $this->entityManager->flush();

        return $apartment;
    }
}