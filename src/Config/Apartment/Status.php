<?php


namespace App\Config\Apartment;

class Status
{
    public const FREE = 'Свободно';

    public CONST SOLD = 'Продано';

    public CONST RESERVATIONS = 'Забронировано';

    public CONST FREE_INT = 1;

    public CONST SOLD_INT = 2;

    public CONST RESERVATIONS_INT = 3;

    public static function getList(): array
    {
        return [
            self::FREE_INT         => self::FREE,
            self::SOLD_INT         => self::SOLD,
            self::RESERVATIONS_INT => self::RESERVATIONS,
        ];
    }

    public static function getAllowedStatusesIds(): array
    {
        return [self::FREE_INT, self::RESERVATIONS_INT];
    }

    public static function getIntByName($name): ?int
    {
        foreach (self::getList() as $key => $value) {
            if ($name === $value) {
                return $key;
            }
        }
        return null;
    }
}
