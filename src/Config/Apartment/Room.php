<?php

namespace App\Config\Apartment;

class Room
{
    public CONST PENTHOUSE = 'Пентхаус';

    /**
     * @return array
     */
    public static function getNamesRooms(): array
    {
        return [
            0   => static::PENTHOUSE,
            1   => 'Однокомнатная',
            2   => 'Двухкомнатная',
            3   => 'Трехкомнатная',
            4   => 'Четырёхкомнатная',
            5   => 'Пятикомнатная',
            6   => 'Шестикомнатная',
            7   => 'Семикомнатная',
            8   => 'Восьмикомнатная',
            9   => 'Девятикомнатная',
            'П' => static::PENTHOUSE,
        ];
    }

    /**
     * @param $quantityRoom
     * @return string|null
     */
    public static function getNameByQuantityRoom($quantityRoom): ?string
    {
        return self::getNamesRooms()[$quantityRoom] ?? null;
    }
}
