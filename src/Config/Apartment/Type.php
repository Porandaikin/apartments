<?php

namespace App\Config\Apartment;

class Type
{
    public const PENTHOUSE = 'penthouse';
    public const ROOMED = 'roomed';

    /**
     * @return array
     */
    public static function getList(): array
    {
        return [
            static::ROOMED    => 'Комнатная',
            static::PENTHOUSE => 'Пентхаус',
        ];
    }

    /**
     * @param string $type
     * @return bool
     */
    public static function isCorrectType(string $type): bool
    {
        return array_key_exists($type, static::getList());
    }
}
