<?php

namespace App\Config\ApartmentLayout;

class Type
{
    public CONST MAIN = 'Главная планировка';

    public CONST WITH_FURNITURE = 'С расстановкой мебели';

    public CONST RENDER_3D = '3D рендер';

    public const OTHER = 'Другое';

    public const MAIN_INT = 1;

    public const WITH_FURNITURE_ID = 2;

    public const RENDER_3D_ID = 3;

    public const OTHER_INT = 4;

    public static function getList(): array
    {
        return [
            self::MAIN_INT          => self::MAIN,
            self::WITH_FURNITURE_ID => self::WITH_FURNITURE,
            self::RENDER_3D_ID      => self::RENDER_3D,
            self::OTHER_INT         => self::OTHER,
        ];
    }

    public static function getNameByInt($id): ?string
    {
        $list = self::getList();

        return $list[$id] ?? '';
    }
}
