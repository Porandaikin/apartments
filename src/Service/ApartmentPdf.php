<?php


namespace App\Service;


use App\Entity\Log;
use Doctrine\ORM\EntityManagerInterface;
use Mpdf\Mpdf;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ApartmentPdf
{
    use ControllerTrait;
    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(ParameterBagInterface $params, ContainerInterface $container, EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->entityManager = $entityManager;
        //TODO рендерить не здесь
        $this->container = $container;
    }

    /**
     * @param array $apartmentData
     * @return string
     */
    public function getPdf(array $apartmentData): string
    {
        try {
            $nameFile = "{$apartmentData['id']}.pdf";
            $mPdf = new Mpdf();
            $mPdf->WriteHTML($this->renderView('pdf/apartment.html.twig', [
                'apartment' => $apartmentData,
            ]));
            $mPdf->Output($this->getPdfFile($nameFile), 'F');
        } catch (\Throwable $exception) {
            $log = (new Log())->setError($exception->getMessage());
            $this->entityManager->persist($log);
            $this->entityManager->flush();
            $nameFile = '';
        }

        return $nameFile;
    }

    /**
     * @param string $nameFile
     * @return string
     */
    public function getPdfFile(string $nameFile): string
    {
        $publicDirectory = $this->params->get('app.path.pdf_dir');
        return $publicDirectory . "/{$nameFile}";
    }
}
