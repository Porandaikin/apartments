<?php

namespace App\Service;

use App\Dto\Emails\RequestCost;
use App\Entity\MailLog;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Mailer
{
    use ControllerTrait;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var ParameterBagInterface
     */
    private $params;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(\Swift_Mailer $mailer, ParameterBagInterface $params, EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->mailer = $mailer;
        $this->params = $params;
        $this->entityManager = $entityManager;
        //TODO рендерить не здесь
        $this->container = $container;
    }

    /**
     * @param RequestCost $requestCost
     * @return MailLog
     */
    public function sendMail(RequestCost $requestCost): MailLog
    {
        $messageBodyHtml = $this->renderView(
        // templates/emails/registration.html.twig
            'emails/request_cost_email.html.twig',
            $requestCost->toArray()
        );
        //TODO Сделать нормальную фабрику
        $mailTo = explode(',', $this->params->get('app.mail_to_sales_department'));
        $mailCc = explode(',', $this->params->get('app.mail_copy_to_sales_department'));
        $message = (new \Swift_Message('Запрос стоимости'))
            ->setSubject('Запрос стоимости с сайта: ' . $this->params->get('app.url_app'))
            ->setFrom($this->params->get('app.mail_from'))
            ->setTo($mailTo)
            ->setCc($mailCc)
            ->setBody($messageBodyHtml, 'text/html');

        $resultSend = $this->mailer->send($message);
        $mailLog = (new MailLog())
            ->setSendTo($this->params->get('app.mail_to_sales_department'))
            ->setSendCc($this->params->get('app.mail_copy_to_sales_department'))
            ->setMessage($messageBodyHtml)
            ->setResultMail($resultSend);
        $this->entityManager->persist($mailLog);
        $this->entityManager->flush();

        return $mailLog;
    }
}
