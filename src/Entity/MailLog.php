<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MailLogRepository")
 * @ORM\HasLifecycleCallbacks
 */
class MailLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="send_to")
     * @var string|null
     */
    private $sendTo;

    /**
     * @ORM\Column(type="string", name="send_cc")
     * @var string|null
     */
    private $sendCc;

    /**
     * @ORM\Column(type="string", name="message")
     * @var string
     */
    private $message;

    /**
     * @ORM\Column(type="integer", name="result_mail")
     * @var integer
     */
    private $resultMail;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     * @var \DateTime|null
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     * @var \DateTime|null
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return MailLog
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return int
     */
    public function getResultMail(): int
    {
        return $this->resultMail;
    }

    /**
     * @param int $resultMail
     * @return MailLog
     */
    public function setResultMail(int $resultMail): self
    {
        $this->resultMail = $resultMail;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return MailLog
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return MailLog
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSendTo(): ?string
    {
        return $this->sendTo;
    }

    /**
     * @param string|null $sendTo
     * @return MailLog
     */
    public function setSendTo(?string $sendTo): self
    {
        $this->sendTo = $sendTo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSendCc(): ?string
    {
        return $this->sendCc;
    }

    /**
     * @param string|null $sendCc
     * @return MailLog
     */
    public function setSendCc(?string $sendCc): self
    {
        $this->sendCc = $sendCc;

        return $this;
    }
}
