<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Table(name="houses")
 * @ORM\Entity(repositoryClass="App\Repository\HouseRepository")
 */
class House
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="type")
     */
    private $type;

    /**
     * @var PersistentCollection
     * @OneToMany(targetEntity="Apartment", mappedBy="house")
     */
    private $apartments;

    /**
     * @ORM\Column(type="integer", name="quantity_floor")
     * @var int
     */
    private $quantityFloor = 1;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return House
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getApartments(): PersistentCollection
    {
        return $this->apartments;
    }

    /**
     * @param Apartment $apartments
     * @return House
     */
    public function addApartments(Apartment $apartments): self
    {
        $this->apartments->add($apartments);

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityFloor(): int
    {
        return $this->quantityFloor;
    }

    /**
     * @param int $quantityFloor
     */
    public function setQuantityFloor(int $quantityFloor): void
    {
        $this->quantityFloor = $quantityFloor;
    }
}
