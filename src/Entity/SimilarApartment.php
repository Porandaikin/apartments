<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Table("similar_apartments")
 * @ORM\Entity(repositoryClass="App\Repository\SimilarApartmentRepository")
 */
class SimilarApartment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="Apartment", inversedBy="features")
     * @JoinColumn(name="parent_id", referencedColumnName="id")
     * @var Apartment
     */
    private $parentApartment;

    /**
     * @ManyToOne(targetEntity="Apartment", inversedBy="features")
     * @JoinColumn(name="similar_apartment_id", referencedColumnName="id")
     * @var Apartment
     */
    private $similarApartment;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Apartment
     */
    public function getParentApartment(): Apartment
    {
        return $this->parentApartment;
    }

    /**
     * @param Apartment $parentApartment
     * @return SimilarApartment
     */
    public function setParentApartment(Apartment $parentApartment): self
    {
        $this->parentApartment = $parentApartment;

        return $this;
    }

    /**
     * @return Apartment
     */
    public function getSimilarApartment(): Apartment
    {
        return $this->similarApartment;
    }

    /**
     * @param Apartment $similarApartment
     * @return SimilarApartment
     */
    public function setSimilarApartment(Apartment $similarApartment): self
    {
        $this->similarApartment = $similarApartment;

        return $this;
    }
}
