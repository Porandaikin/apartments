<?php

namespace App\Entity;

use App\Config\Apartment\Status;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="apartments")
 * @ORM\Entity(repositoryClass="App\Repository\ApartmentRepository")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 */
class Apartment
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", name="title")
     */
    private $title = '';

    /**
     * @ORM\Column(type="float", name="area")
     */
    private $area = 0;

    /**
     * @ORM\Column(type="integer", name="quantity_room")
     */
    private $quantityRoom = 1;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('roomed', 'penthouse)")
     */
    private $type = 'roomed';

    /**
     * @ORM\Column(type="integer", name="floor")
     */
    private $floor = 1;

    /**
     * @ORM\Column(type="float", name="cost_meter")
     */
    private $costMeter = 0;

    /**
     * @ORM\Column(type="float", name="total_cost")
     */
    private $totalCost = 0;

    /**
     * @ORM\Column(type="string", name="placing_apartment_on_quarter_map")
     */
    private $placingApartmentOnQuarterMap = '';

    /**
     * @Vich\UploadableField(mapping="apartment_placing_map", fileNameProperty="placingApartmentOnQuarterMap")
     * @var File
     */
    private $placingApartmentOnQuarterMapFile;

    /**
     * @ORM\Column(type="string", name="placing_apartment_on_floor_plan")
     */
    private $placingApartmentOnFloorPlan = '';

    /**
     * @Vich\UploadableField(mapping="apartment_placing_floor", fileNameProperty="placingApartmentOnFloorPlan")
     * @var File
     */
    private $placingApartmentOnFloorPlanFile;

    /**
     * @ORM\Column(type="boolean", name="show_in_preview")
     */
    private $showInPreview = true;

    /**
     * @ManyToOne(targetEntity="House")
     * @JoinColumn(name="house_id", referencedColumnName="id")
     */
    private $house;

    /**
     * @OneToMany(targetEntity="ApartmentLayouts", mappedBy="apartment")
     * @var Collection
     */
    private $apartmentLayouts;

    /**
     * @OneToMany(targetEntity="SimilarApartment", mappedBy="parentApartment")
     * @var Collection
     */
    private $similarApartments;

    /**
     * @ORM\Column(type="string", name="path_to_pdf")
     * @var string|null
     */
    private $pathToPdf;

    /**
     * @ORM\Column(type="integer", name="status")
     * @var int
     */
    private $status = Status::FREE_INT;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->similarApartments = new ArrayCollection();
        $this->apartmentLayouts = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new \DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Apartment
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Apartment
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return float
     */
    public function getArea(): float
    {
        return $this->area;
    }

    /**
     * @param float $area
     * @return Apartment
     */
    public function setArea(float $area): self
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantityRoom(): int
    {
        return $this->quantityRoom;
    }

    /**
     * @param int $quantityRoom
     * @return Apartment
     */
    public function setQuantityRoom(int $quantityRoom): self
    {
        $this->quantityRoom = $quantityRoom;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Apartment
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getFloor(): int
    {
        return $this->floor;
    }

    /**
     * @param int $floor
     * @return Apartment
     */
    public function setFloor(int $floor): self
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * @return float
     */
    public function getCostMeter(): float
    {
        return $this->costMeter;
    }

    public function getCostMeterString(): string
    {
        return number_format($this->costMeter, 0, ',', ' ');
    }

    /**
     * @param float $costMeter
     * @return Apartment
     */
    public function setCostMeter(float $costMeter): self
    {
        $this->costMeter = $costMeter;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotalCost(): float
    {
        return $this->totalCost;
    }

    /**
     * @return string
     */
    public function getTotalCostString(): string
    {
        return number_format($this->totalCost, 0, ',', ' ');
    }

    /**
     * @param float $totalCost
     * @return Apartment
     */
    public function setTotalCost(float $totalCost): self
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlacingApartmentOnQuarterMap(): ?string
    {
        return $this->placingApartmentOnQuarterMap;
    }

    /**
     * @param string $placingApartmentOnQuarterMap
     * @return Apartment
     */
    public function setPlacingApartmentOnQuarterMap(string $placingApartmentOnQuarterMap = null): self
    {
        $this->placingApartmentOnQuarterMap = $placingApartmentOnQuarterMap;

        return $this;
    }

    /**
     * @return string
     */
    public function getPlacingApartmentOnFloorPlan(): ?string
    {
        return $this->placingApartmentOnFloorPlan;
    }

    /**
     * @param string $placingApartmentOnFloorPlan
     * @return Apartment
     */
    public function setPlacingApartmentOnFloorPlan(string $placingApartmentOnFloorPlan = null): self
    {
        $this->placingApartmentOnFloorPlan = $placingApartmentOnFloorPlan;

        return $this;
    }

    /**
     * @return bool
     */
    public function getShowInPreview(): bool
    {
        return $this->showInPreview;
    }

    /**
     * @param $showInPreview
     * @return Apartment
     */
    public function setShowInPreview($showInPreview): self
    {
        $this->showInPreview = $showInPreview;

        return $this;
    }

    /**
     * @return House
     */
    public function getHouse(): ?House
    {
        return $this->house;
    }

    /**
     * @param House $house
     * @return Apartment
     */
    public function setHouse(House $house): self
    {
        $this->house = $house;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getApartmentLayouts(): Collection
    {
        return $this->apartmentLayouts;
    }

    /**
     * @param ApartmentLayouts $apartmentLayouts
     * @return Apartment
     */
    public function addApartmentLayouts(ApartmentLayouts $apartmentLayouts): self
    {
        $this->apartmentLayouts->add($apartmentLayouts);

        return $this;
    }

    /**
     * @param SimilarApartment $similarApartments
     * @return Apartment
     */
    public function addSimilarApartments(SimilarApartment $similarApartments): self
    {
        $this->similarApartments->add($similarApartments);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getSimilarApartments(): Collection
    {
        return $this->similarApartments;
    }

    /**
     * @return string|null
     */
    public function getPathToPdf(): ?string
    {
        return $this->pathToPdf;
    }

    /**
     * @param string|null $pathToPdf
     * @return Apartment
     */
    public function setPathToPdf(?string $pathToPdf): self
    {
        $this->pathToPdf = $pathToPdf;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Apartment
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return File
     */
    public function getPlacingApartmentOnQuarterMapFile(): ?File
    {
        return $this->placingApartmentOnQuarterMapFile;
    }

    /**
     * @param File $placingApartmentOnQuarterMapFile
     * @return Apartment
     * @throws \Exception
     */
    public function setPlacingApartmentOnQuarterMapFile(File $placingApartmentOnQuarterMapFile = null): self
    {
        $this->placingApartmentOnQuarterMapFile = $placingApartmentOnQuarterMapFile;

        if ($placingApartmentOnQuarterMapFile) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Apartment
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Apartment
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return File
     */
    public function getPlacingApartmentOnFloorPlanFile(): ?File
    {
        return $this->placingApartmentOnFloorPlanFile;
    }

    /**
     * @param File $placingApartmentOnFloorPlanFile
     * @return Apartment
     * @throws \Exception
     */
    public function setPlacingApartmentOnFloorPlanFile(File $placingApartmentOnFloorPlanFile = null): self
    {
        $this->placingApartmentOnFloorPlanFile = $placingApartmentOnFloorPlanFile;

        if ($placingApartmentOnFloorPlanFile) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }
}
